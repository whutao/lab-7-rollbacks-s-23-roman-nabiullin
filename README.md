# Lab7 - Rollbacks and reliability

## Homework - Part 1

The problem was in the following: `buy_decrease_balance` and `buy_decrease_stock` were handled as a separate queries. They should be performed in a single transaction so that it can be rolled back in case of a failure

## Homework - Part 2

Create inventory table with amount constraint.

```sql
CREATE TABLE Inventory(
    username TEXT UNIQUE,
    product TEXT UNIQUE,
    amount INT CHECK(amount >= 0),
    CONSTRAINT inventory_pk PRIMARY KEY (username, product),
    CONSTRAINT inventory_limit CHECK (amount <= 100)
);
```

New queries:

```sql
SELECT SUM(amount) FROM Inventory WHERE username = %(username)s;
```

```sql
INSERT INTO Inventory (username, product, amount)
VALUES (%(username)s, %(product)s, %(amount)s)
ON CONFLICT (username, product)
DO UPDATE SET amount = inventory.amount + EXCLUDED.amount;
```

## Extra 1

To solve this problem, we can persist a table of completed transactions

```sql
CREATE TABLE Transactions(
    transaction_id TEXT UNIQUE
);
```

and we add new query

```sql
'INSERT INTO Transactions (transaction_id) VALUES (%s)'
```

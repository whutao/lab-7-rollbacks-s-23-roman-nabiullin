import psycopg2
import random
import string


price_request = 'SELECT price FROM Shop WHERE product = %(product)s'
buy_decrease_balance = f'UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s'
buy_decrease_stock = 'UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s'
user_inventory_request = 'SELECT SUM(amount) FROM Inventory WHERE username = %(username)s'
update_inventory_request = 'INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = inventory.amount + EXCLUDED.amount'
write_transaction_request = 'INSERT INTO Transactions (transaction_id) VALUES (%s)'

def transaction(connection):
    class _Transaction:
        def __init__(self, connection):
            self.connection = connection
            self.cursor = self.connection.cursor()

        def __enter__(self):
            self.cursor.execute('BEGIN')
            return self.cursor

        def __exit__(self, exc_type, exc_value, traceback):
            if exc_type is None:
                self.cursor.execute('COMMIT')
            else:
                self.cursor.execute('ROLLBACK')
            self.cursor.close()
    
    return _Transaction(connection)


def get_connection():
    return psycopg2.connect(
        dbname='sqr-lab7',
        user='postgres',
        password='postgres',
        host='localhost',
        port=5432
    )


def buy_product(username, product, amount):
    obj = {
        'product': product,
        'username': username,
        'amount': amount
    }
    new_transaction_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    with get_connection() as connection:
        with transaction(connection) as cursor:
            cursor.execute(buy_decrease_balance, (obj,))
            if cursor.rowcount != 1:
                raise Exception('Wrong username')   

            cursor.execute(buy_decrease_stock, (obj,))
            if cursor.rowcount != 1:
                raise Exception('Wrong product or out of stock')            
        
            cursor.execute(user_inventory_request, (obj,))

            total_inventory = cursor.fetchone()
            if total_inventory is None:
                total_inventory = 0
            elif total_inventory[0] is None:
                total_inventory = 0 
            else:
                total_inventory = total_inventory[0]
            
            cursor.execute(update_inventory_request, (obj, ))
            if cursor.rowcount != 1:
                raise Exception('Cannot update inventory')
            
            cursor.execute(write_transaction_request, (new_transaction_id, ))
